/**************************************************************************
   Copyright (C) Axel Latvala - All Rights Reserved
   Unauthorized copying of this file, via any medium is strictly prohibited
   Proprietary and confidential
   Written by Axel Latvala <softwarelicense@alatvala.fi>, December 2017
 **************************************************************************/

#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <WebSocketsServer.h>
#include "FS.h"
#include <ArduinoJson.h>

#define SYSNAME "Vonlatvala WifiRGB"
#define HTTPPORT 80
#define WEBSOCKETPORT 81
#define USE_SERIAL Serial
#define MAX_CONFIG_FILESIZE 2048

ESP8266WebServer server(80);
WebSocketsServer webSocket = WebSocketsServer(WEBSOCKETPORT);

String wifiSsid;
String wifiPassword;
String hostName;

void setup() {
  USE_SERIAL.begin(115200);

  USE_SERIAL.println(String("\n") + String(SYSNAME) + String(" booting, please stand by..."));

  USE_SERIAL.print(String("Mounting FS..."));
  if(SPIFFS.begin()) {
    USE_SERIAL.println("OK!");
  } else {
    USE_SERIAL.println("FAILED.");
  }

  hostName = String(ESP.getChipId());
  wifiSsid = String("NO_CONFIG");
  wifiPassword = String("NOCONFIG");

  USE_SERIAL.print("Checking for existing configuration on FS...");
  if(SPIFFS.exists("/config.json")) {
    USE_SERIAL.println("exists.");
    USE_SERIAL.print("Loading configuration from FS...");
    File configFh = SPIFFS.open("/config.json", "r");
    if(!configFh) {
      USE_SERIAL.println("FAILED.");
    } else {
      USE_SERIAL.println("OK!");
      USE_SERIAL.print("Filesize of our config: ");
      size_t configSize = configFh.size();
      USE_SERIAL.print(configSize);
      if(configSize <= MAX_CONFIG_FILESIZE) {
        USE_SERIAL.println(String("which IS within limits (") + String(MAX_CONFIG_FILESIZE) + String("), proceeding."));
      } else {
        USE_SERIAL.println(String("which IS NOT within limits(") + String(MAX_CONFIG_FILESIZE) + String("), ABORTING."));
      }
      USE_SERIAL.print("Parsing configuration...");
      std::unique_ptr<char[]> buf(new char[configSize]);
      configFh.readBytes(buf.get(), configSize);

      StaticJsonBuffer<MAX_CONFIG_FILESIZE> jsonBuffer;
      JsonObject& configJson = jsonBuffer.parseObject(buf.get());

      if(configJson.success()) {
        USE_SERIAL.println("OK!");      
        wifiSsid = String((const char*)configJson["wifi"]["ssid"]);
        wifiPassword = String((const char*)configJson["wifi"]["password"]);
        hostName = String((const char*)configJson["hostname"]);
      } else {
        USE_SERIAL.println("FAILED.");
      }
    }
  }
  
  WiFi.begin(wifiSsid.c_str(), wifiPassword.c_str());

  USE_SERIAL.print(String("Connecting to WiFi (") + String(wifiSsid) + String(")"));

  while (WiFi.status() != WL_CONNECTED) {
    delay(250);
    USE_SERIAL.print(".");
  }

  USE_SERIAL.println("OK!");
  USE_SERIAL.print("IP address: ");
  USE_SERIAL.println(WiFi.localIP());

  String mdnsName = hostName;

  USE_SERIAL.print(String("Starting MDNS responder for '") + mdnsName + String("'..."));
  if(MDNS.begin(mdnsName.c_str())) {
    USE_SERIAL.print(".");
    MDNS.addService("http", "tcp", 80);
    USE_SERIAL.print(".");
    MDNS.addService("ws", "tcp", WEBSOCKETPORT);
    USE_SERIAL.println("OK!");
  } else {
    USE_SERIAL.println("FAIL");
  }

  USE_SERIAL.print(String("Initializing HTTP Service on port ") + String(HTTPPORT) + String("..."));
  server.on("/", handleRoot);
  USE_SERIAL.print(".");
  server.onNotFound(handleNotFound);
  USE_SERIAL.print(".");
  server.begin();
  USE_SERIAL.print(".");
  USE_SERIAL.println("OK!");

  USE_SERIAL.print(String("Initializing WebSocketServer on port ") + String(WEBSOCKETPORT) + String("..."));
  webSocket.begin();
  USE_SERIAL.print(".");
  webSocket.onEvent(webSocketEvent);
  USE_SERIAL.println("OK!");

  USE_SERIAL.println("\nALL SYSTEMS INITIALIZED\n");

}


void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += ( server.method() == HTTP_GET ) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";

  for ( uint8_t i = 0; i < server.args(); i++ ) {
    message += " " + server.argName ( i ) + ": " + server.arg ( i ) + "\n";
  }

  server.send ( 404, "text/plain", message );
}

void handleRoot() {
  char temp[400];
  int sec = millis() / 1000;
  int min = sec / 60;
  int hr = min / 60;

  snprintf(temp,400,
             "<html>\
  <head>\
    <meta http-equiv='refresh' content='5'/>\
    <title>ESP8266 Webserver</title>\
    <style>\
      body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
    </style>\
  </head>\
  <body>\
    <h1>ESP8266 webserver up and running!</h1>\
    <p>Uptime: %02d:%02d:%02d</p>\
  </body>\
</html>",

             hr, min % 60, sec % 60
           );
  server.send ( 200, "text/html", temp );
}

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {
  switch (type) {
    case WStype_DISCONNECTED:
      USE_SERIAL.printf("[%u] Disconnected!\n", num);
      break;
    case WStype_CONNECTED: {
        IPAddress ip = webSocket.remoteIP(num);
        USE_SERIAL.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, 
                          ip[0], ip[1], ip[2], ip[3], payload);

        // send message to client
        webSocket.sendTXT(num, "Connected");
      }
      break;
    case WStype_TEXT:
      USE_SERIAL.printf("[%u] get Text: %s\n", num, payload);

      // send message to client
      webSocket.sendTXT(num, "Hello there ^_^");

      // send data to all connected clients
      webSocket.broadcastTXT("You are all my buddies!");
      break;
    case WStype_BIN:
      USE_SERIAL.printf("[%u] get binary length: %u\n", num, length);
      hexdump(payload, length);

      // send message to client
      // webSocket.sendBIN(num, payload, length);
      break;
  }
}

void loop() {
  webSocket.loop();
  server.handleClient();
}
